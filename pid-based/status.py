#!/usr/bin/env python3

import sys
import os.path
import re
from typing import NamedTuple, Optional, List
from time import time
import subprocess

CLK_TCK = int(subprocess.run(["getconf", "CLK_TCK"], check=True, capture_output=True).stdout)
with open("/proc/stat") as fh:
	m = re.search(r"^btime ([0-9]+)$", fh.read(), re.MULTILINE)
	assert m
	boottime = int(m.group(1))
	uptime = time() - boottime

ANOMALOUS_TIME_THRESHOLD_SECONDS = 30
OK_STATES = {"run"}

TERM_RED   = "\033[31m"
TERM_GREEN = "\033[32m"
TERM_YELLW = "\033[33m"
TERM_BOLD  = "\033[1m"
TERM_ITAL  = "\033[3m"
TERM_ULINE = "\033[4m"
TERM_RESET = "\033[0m"


class Status(NamedTuple):
	name:  str
	state: str
	time:  Optional[int]
	pid:   Optional[int]
	pid_file:  str
	anomalous_state: bool
	anomalous_time:  bool


def get_status_for_pid_file(filename):
	name = os.path.basename(filename)
	name = re.sub(r"\.pid$", "", name)

	with open(filename) as fh:
		try:
			pid = int(fh.read())
		except:
			pid = None

	time = None

	try:
		with open(f"/proc/{pid}/stat") as fh:
			line = fh.readline()
			m = re.match(r"""
				[0-9-]+[ ]         # (1) pid
				\(.{,16}\)[ ]      # (2) comm
				([RSDZTtX])[ ]     # (3) state
				(?:[0-9-]+[ ]){18} # skip 18 fields
				([0-9]+)[ ]        # (22) starttime
			""", line, re.VERBOSE)
			assert m

			state = {
				"R": "run",
				"S": "run",
				"D": "run",
				"Z": "zombie",
				"T": "run",
				"t": "run",
				"X": "dead",
			}[m.group(1)]

			# running time = time since boot now - time since boot at program start
			runningtime = int(uptime - int(m.group(2)) / CLK_TCK)
	except FileNotFoundError:
		state = "down"
		runningtime = None

	anomalous_state  = state not in OK_STATES
	anomalous_time   = runningtime is not None and runningtime < ANOMALOUS_TIME_THRESHOLD_SECONDS

	return Status(
		name   = name,
		state  = state,
		time   = runningtime,
		pid    = pid,
		pid_file  = filename,
		anomalous_state = anomalous_state,
		anomalous_time  = anomalous_time
	)



def format_time(total_seconds):
	if total_seconds is None:
		return ""
	if total_seconds < 60:
		return f"{total_seconds}s"
	total_minutes, seconds = divmod(total_seconds, 60)
	if total_minutes < 60:
		return f"{total_minutes}m{seconds:02}s"
	total_hours, minutes = divmod(total_minutes, 60)
	if total_hours < 24:
		return f"{total_hours}h{minutes:02}m"
	total_days, hours = divmod(total_hours, 24)
	return f"{total_days}d{hours:02}h"


def status_format(s, name_column_len):
	bullet = (
		f"{TERM_RED  }🞫{TERM_RESET}" if s.anomalous_state else
		f"{TERM_YELLW}●{TERM_RESET}" if s.state == "run" and s.anomalous_time else
		f"{TERM_GREEN}●{TERM_RESET}" if s.state == "run" else
		f"{TERM_GREEN}○{TERM_RESET}" if s.state == "down" else
		" "
	)

	return (
		f"{s.name:>{name_column_len}s} "
		f"{bullet} {TERM_BOLD if s.anomalous_state else ''}{s.state:7s}{TERM_RESET}  "
		f"{TERM_BOLD if s.anomalous_time else ''}{format_time(s.time):>6s}{TERM_RESET}  "
		f"{str(s.pid if s.pid is not None else ''):6s}"
	)


def main():
	only_anomalous = False
	try:
		if sys.argv[1] in ["-h", "--help"]:
			print(f"""
status: show that status of PID-tracked processes at a glance
Usage: cat /path/to/*.pid | {sys.argv[0]} [-q]

Options:
  -q, --only-anomalous         Hide services where everything looks okay
  -h, --help                   Show this help text and exit

Symbols in output:
  {TERM_GREEN}○{TERM_RESET} down
  {TERM_GREEN}●{TERM_RESET} run
  {TERM_YELLW}●{TERM_RESET} run but not for long
  {TERM_RED  }🞫{TERM_RESET} something's off
			""".strip())
			return
		elif sys.argv[1] in ["-q", "--only-anomalous"]:
			only_anomalous = True
	except IndexError:
		pass

	statuses = []
	for line_no, line in enumerate(sys.stdin, start=1):
		try:
			statuses.append(get_status_for_pid_file(line.rstrip("\n")))
		except Exception as exc:
			raise Exception(f"Problem with line {line_no}: {line.rstrip()}") from exc

	statuses_to_print = [s for s in statuses if s.anomalous] if only_anomalous else statuses

	if statuses_to_print:
		name_column_len = max(
			10,
			max((len(s.name) for s in statuses_to_print), default=0)
		)

		# Print header
		print(f"{TERM_ULINE}{'Service':>{name_column_len}s}   State     Since  PID   {TERM_RESET}")

		for status in statuses_to_print:
			print(status_format(status, name_column_len=name_column_len))


if __name__ == '__main__':
	main()
